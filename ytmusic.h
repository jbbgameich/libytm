// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#ifndef YTMUSIC_H
#define YTMUSIC_H

#include <string>
#include <optional>
#include <map>
#include <variant>
#include <vector>
#include <memory>

struct YTMusicPrivate;

namespace meta {
struct Thumbnail {
    const std::string url;
    const int width;
    const int height;
};
struct Artist {
    const std::string name;
    const std::optional<std::string> id;
};
struct Album {
    const std::string name;
    const std::string id;
};
}


namespace search {
///
/// Base class for Video and Song, which share most attributes.
///
struct Media {
    const std::string video_id;
    const std::string title;
    const std::vector<meta::Artist> artists;
    const std::string duration;
};

struct Video : public Media {
    const std::string views;
};

struct Playlist {
    const std::string browse_id;
    const std::string title;
    const std::string author;
    const std::string item_count;
};

struct Song : public Media {
    const meta::Album album;
    const bool is_explicit;
};

struct Album {
    const std::string browse_id;
    const std::string title;
    const std::string type;
    const std::vector<meta::Artist> artists;
    const std::string year;
    const bool is_explicit;
};

struct Artist {
    const std::string browse_id;
    const std::string artist;
    const std::string shuffle_id;
    const std::string radio_id;
};

using SearchResultItem = std::variant<Video, Playlist, Song, Album, Artist>;
};


namespace artist {
struct Artist {
    template<typename T>
    struct Section {
        const std::optional<std::string> browse_id;
        const std::vector<T> results;
        const std::optional<std::string> params;
    };

    struct Song {
        struct Album {
            const std::string name;
            const std::string id;
        };

        const std::string video_id;
        const std::string title;
        const std::vector<meta::Thumbnail> thumbnails;
        const std::vector<meta::Artist> artist;
        const Album album;
    };

    struct Album {
        const std::string title;
        const std::vector<meta::Thumbnail> thumbnails;
        const std::string year;
        const std::string browseId;
        const std::optional<std::string> type;
    };

    struct Video {
        const std::string title;
        const std::vector<meta::Thumbnail> thumbnails;
        const std::optional<std::string> views;
        const std::string videoId;
        const std::string playlist_id;
    };

    struct Single {
        const std::string title;
        const std::vector<meta::Thumbnail> thumnnails;
        const std::string year;
        const std::string browseId;
    };

    const std::optional<std::string> description;
    const std::optional<std::string> views;
    const std::string name;
    const std::string channel_id;
    const std::string subscribers;
    const bool subscribed;
    const std::vector<meta::Thumbnail> thumbnails;
    const std::optional<Section<Song>> songs;
    const std::optional<Section<Album>> albums;
    const std::optional<Section<Single>> singles;
    const std::optional<Section<Video>> videos;
};
}

namespace album {
    struct Track {
        const std::string index;
        const std::string title;
        const std::string artists;
        const std::string video_id;
        const std::string length_ms;
        const std::string like_status;
    };

    struct Album {
        struct ReleaseDate {
            int year;
            int month;
            int day;
        };

        const std::string title;
        const std::string track_count;
        const std::string duration_ms;
        const std::string playlist_id;
        const ReleaseDate release_date;
        const std::string description;
        const std::vector<meta::Thumbnail> thumbnails;
        const std::vector<Track> tracks;
    };
}

namespace song {
struct Song {
    struct Thumbnail {
        std::vector<meta::Thumbnail> thumbnails;
    };

    const std::string video_id;
    const std::string title;
    const std::string length;
    const std::vector<std::string> keywords;
    const std::string channel_id;
    const bool is_owner_viewer;
    const std::string short_description;
    const bool is_crawlable;
    const Thumbnail thumbnail;
    const float average_rating;
    const bool allow_ratings;
    const std::string view_count;
    const std::string author;
    const bool is_private;
    const bool is_unplugged_corpus;
    const bool is_live_content;
    const std::string provider;
    const std::vector<std::string> artists;
    const std::string copyright;
    const std::vector<std::string> production;
    const std::string release;
    const std::string category;
};
}

namespace playlist {
struct Track {
    const std::optional<std::string> video_id;
    const std::string title;
    const std::vector<meta::Artist> artists;
    const std::optional<meta::Album> album;
    const std::string duration;
    const std::optional<std::string> like_status;
    const std::vector<meta::Thumbnail> thumbnails;
    const bool is_available;
    const bool is_explicit;
};

struct Playlist {
    const std::string id;
    const std::string privacy;
    const std::string title;
    const std::vector<meta::Thumbnail> thumbnails;
    const meta::Artist author;
    const std::string year;
    const std::string duration;
    const int track_count;
    const std::vector<Track> tracks;
};
}

class YTMusic
{
public:
    YTMusic(const std::optional<std::string> &auth = std::nullopt,
            const std::optional<std::string> &user = std::nullopt,
            const std::optional<bool> requests_session = std::nullopt,
            const std::optional<std::map<std::string, std::string>> &proxies = std::nullopt,
            const std::string &language = "en");

    ~YTMusic();

    std::vector<search::SearchResultItem> search(const std::string &query,
                                                 const std::optional<std::string> &filter = std::nullopt,
                                                 const int limit = 20,
                                                 const bool ignore_spelling = false) const;

    /// https://ytmusicapi.readthedocs.io/en/latest/reference.html#ytmusicapi.YTMusic.get_artist
    artist::Artist get_artist(const std::string &channelId) const;

    /// https://ytmusicapi.readthedocs.io/en/latest/reference.html#ytmusicapi.YTMusic.get_album
    album::Album get_album(const std::string &browse_id) const;

    /// https://ytmusicapi.readthedocs.io/en/latest/reference.html#ytmusicapi.YTMusic.get_song
    song::Song get_song(const std::string &video_id) const;

    /// https://ytmusicapi.readthedocs.io/en/latest/reference.html#ytmusicapi.YTMusic.get_playlist
    playlist::Playlist get_playlist(const std::string &playlist_id, int limit = 100) const;

    /// https://ytmusicapi.readthedocs.io/en/latest/reference.html#ytmusicapi.YTMusic.get_artist_albums
    std::vector<artist::Artist::Album> get_artist_albums(const std::string &channel_id, const std::string &params) const;

    // TODO wrap more methods

private:
    std::unique_ptr<YTMusicPrivate> d;
};

#endif // YTMUSIC_H
